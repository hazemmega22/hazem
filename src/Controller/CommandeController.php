<?php

namespace App\Controller;


use App\Entity\Commande;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType ;
use Symfony\Component\Form\Extension\Core\Type\TextType ;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class CommandeController extends AbstractController
{
    /**
     * @Route("/commande/list", name="commande")
     */
    public function index(): Response
    {
        $form = $this->getDoctrine()
        ->getRepository(Commande::class)
        ->findAll();


    return $this->render('commande/list.html.twig', [
        'form' => $form,
    ]);
      
    }

    



      /**
     * @Route("/commande/add" ,name="new_list")
     *Method({"GET", "POST"})
     */
    public function new(Request $request)
    {
       $Commande = new Commande();
       $form = $this->createformbuilder($Commande)
       
       ->add('Date',DateType::class)
       ->add('nom_client',TextType::class)
       ->add('prenom_client',TextType::class)
           ->add('telephone',TextType::class)
           ->add('adresse',TextType::class)
           ->add('montant',TextType::class)
           ->add('etat_commande',IntegerType::class)
           ->add('mode_paiement',TextType::class)


       
        
       ->add('add',SubmitType::class)
       ->getForm();

       $form->handleRequest($request);
       if ($form->isSubmitted() && $form->isValid()) {
           $Commande= $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Commande);
            $entityManager->flush();
            return $this->redirectToRoute('list');
        }
        return $this->render('commande/add.html.twig', [
            'form' => $form->createView() 
        ]);
    
     }


   /**
     * @Route("/Commande/Delete/{id}" ,name="Delete_Commande")
     *Method({"DELETE"})
     */
    public function Delete(Request $request,$id)
    {
            $Commande = $this->getDoctrine()
            ->getRepository(Commande::class)
            ->find($id);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($Commande);
            $entityManager->flush();

            
            return $this->redirectToRoute('list');
    }

    /**
     * @Route("/commandet/edit/{id}",name="update_Commande")
     * Method({"GET", "POST"})
     */
    public function update($id, Request $request)
    {
        $Commande = new Commande();
        $Commande = $this->getDoctrine()
            ->getRepository(Commande::class)
            ->find($id);

        $form = $this->createformbuilder($Commande)
        ->add('EtatCmd',TextType::class)
        ->add('Edit',SubmitType::class)
        ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->flush();
             return $this->redirectToRoute('list');
         }
         return $this->render('commande/update.html.twig', [
             'form' => $form->createView()
            ]);
    }
    
}

